import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import vuetify from '@/plugins/vuetify'
import '@/plugins/vuelidate'
import '@/plugins/moment'
import { createProvider } from './vue-apollo'
import { errorHandler } from '@/utils'

Vue.config.productionTip = false
Vue.config.errorHandler = errorHandler

new Vue({
  router,
  store,
  vuetify,
  //apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')
