import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import { ApolloLink } from 'apollo-link'

const AUTH_TOKEN = 'apollo-token'

const link = new HttpLink({
  uri: 'http://192.168.99.100:4000'
})

const authLink = new ApolloLink((operation, forward) => {
  const { headers } = operation.getContext()
  operation.setContext({
    headers: {
      ...headers,
      'Authorization': `Bearer ${window.localStorage.getItem(AUTH_TOKEN)}`
    }
  })
  return forward(operation)
})

const apollo = new ApolloClient({
  cache: new InMemoryCache(),
  link: ApolloLink.from([
    authLink,
    link
  ]),
  connectToDevTools: process.env.NODE_ENV !== 'production'
})

/*const onLogout = async apollo => {
  if (typeof window.localStorage !== 'undefined') {
    window.localStorage.removeItem(AUTH_TOKEN)
  }
  await apollo.re
}*/

export default apollo

export {
  AUTH_TOKEN
}
